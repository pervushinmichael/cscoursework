package app.controllers;

import java.awt.*;
import java.util.List;

import app.services.*;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class MainController {

    @FXML
    private Canvas dCanvas;

    @FXML
    private Button generateButton;

    @FXML
    private Button solveButton;

    @FXML
    private TextField nodesField;

    @FXML
    private Button clearButton;

    @FXML
    private TextField iterationsField;

    @FXML
    private TextField threadsField;

    @FXML
    void initialize() {
        generateButton.setOnAction(actionEvent -> generate());
        solveButton.setOnAction(actionEvent -> drawRoute());
        clearButton.setOnAction(actionEvent -> clearArea());
    }

    private final int WIDTH = 640;
    private final int HEIGHT = 640;

    private GraphGenerator graphGenerator;

    private boolean isGenerated = false;
    private boolean isRouteDrawn = false;

    private void drawRoute() {
        if (!isGenerated) return;
        try {
            int threads = Integer.parseInt(threadsField.getText());
            int iterations = Integer.parseInt(iterationsField.getText());
            if (threads <= 0) throw new NumberFormatException("threads number should be > 0");
            if (iterations < threads) throw new NumberFormatException("iterations number should be >= threads number");
            if (isRouteDrawn) {
                clearArea();
                drawPoints();
            }

            GraphicsContext gc = dCanvas.getGraphicsContext2D();
            AntColonyTSP ant = new AntColonyTSP(graphGenerator.getGraph(), threads);
            long before = System.currentTimeMillis();
            ant.solveMT(iterations);
            long after = System.currentTimeMillis();
            System.out.println(ant.getBestCost());
            System.out.println("time: " + (after - before) + " ms");

            gc.setLineWidth(1);

            List<Integer> path = ant.getBestPath();
            for (int i = 0; i < path.size() - 1; i++) {
                Point p1 = graphGenerator.getPoints().get(path.get(i));
                Point p2 = graphGenerator.getPoints().get(path.get(i + 1));
                gc.strokeLine(p1.x + 2, p1.y + 2, p2.x + 2, p2.y + 2);
            }
            isRouteDrawn = true;
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    private void clearArea() {
        GraphicsContext gc = dCanvas.getGraphicsContext2D();
        gc.clearRect(0, 0, WIDTH, HEIGHT);
        if (isGenerated) isRouteDrawn = true;
    }

    private void generate() {
        try {
            int nodes = Integer.parseInt(nodesField.getText());
            graphGenerator = new GraphGenerator(WIDTH, nodes);
            graphGenerator.generate();
            if (isGenerated) clearArea();
            drawPoints();
            isGenerated = true;
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
    }

    private void drawPoints() {
        GraphicsContext gc = dCanvas.getGraphicsContext2D();

        // points drawing
        for (Point point : graphGenerator.getPoints()) {
            gc.fillOval(point.x, point.y, 5, 5);
        }
    }
}

package app.services;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * Implementation of Multithreading TSP solver by ant colony optimization algorithm
 *
 * @author Michael Pervushin
 * @version 1.0
 */

public class AntColonyTSP {
    private double[][] graph;
    private int nodes;
    private double[][] pheromone;
    private final double pheromoneRate = 0.7;
    private final double Q = 500;
    private final double pheromoneInit = 0.2;
    private List<Integer> bestPath;
    private volatile double bestCost;

    private final int THREADS;
    private Semaphore semaphore;

    /**
     * @param graph   graph given by adjacency matrix
     * @param THREADS threads number
     */
    public AntColonyTSP(double[][] graph, int THREADS) {
        this.graph = graph;
        nodes = graph.length;

        this.THREADS = THREADS;
        semaphore = new Semaphore(THREADS);

        //set start pheromones value
        pheromone = new double[nodes][nodes];
        for (double[] arr : pheromone) Arrays.fill(arr, pheromoneInit);
        bestPath = new ArrayList<>();

        //init best path
        for (int i = 0; i < nodes; i++) bestPath.add(i);
        bestPath.add(0);
        bestCost = getPathCost(bestPath);
    }

    /**
     * @param i         start node
     * @param j         finish node
     * @param freeNodes array of available nodes from i node
     * @return chance to move from i node to j node
     */
    private double getChance(int i, int j, boolean[] freeNodes) {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        double desireToMove = pheromone[i][j] / graph[i][j];
        double desiresToMove = 0;
        for (int k = 0; k < freeNodes.length; k++) {
            if (freeNodes[k]) desiresToMove +=
                    pheromone[i][k] / graph[i][k];
        }
        semaphore.release();
        return desireToMove / desiresToMove;
    }

    /**
     * @param curNode   current node in path
     * @param freeNodes available nodes from current node
     * @return next node index
     */
    private int getNextNode(int curNode, boolean[] freeNodes) {
        //calculate movement chance from current node to all another
        double[] probabilities = new double[nodes];
        for (int i = 0; i < nodes; i++) {
            if (freeNodes[i]) {
                probabilities[i] = i > 0 ? getChance(curNode, i, freeNodes) + probabilities[i - 1] :
                        getChance(curNode, i, freeNodes);
            } else {
                probabilities[i] = i > 0 ? probabilities[i - 1] : 0;
            }
        }
        //choosing next node
        SecureRandom random = new SecureRandom();
        double rndVal = random.nextDouble();

        int nextNode = -1;

        for (int i = 0; i < nodes; i++) {
            if (probabilities[i] > rndVal) {
                nextNode = i;
                break;
            }
        }
        return nextNode;
    }

    /**
     * @param path some path
     * @return path cost
     */
    private double getPathCost(List<Integer> path) {
        double result = 0;
        for (int i = 0; i < path.size() - 1; i++) {
            result += graph[path.get(i)][path.get(i + 1)];
        }
        return result;
    }

    /**
     * if received path is shorter, then it will be saved as best path
     *
     * @param path some path
     * @param cost path cost
     */
    private synchronized void saveBest(List<Integer> path, double cost) {
        if (cost >= bestCost) return;
        bestCost = cost;
        bestPath = path;
    }

    /**
     * @return best path as nodes list
     */
    public List<Integer> getBestPath() {
        return bestPath;
    }

    /**
     * @return summary cost of best path
     */
    public double getBestCost() {
        return bestCost;
    }

    /**
     * Method performs the specified number of iterations to solve TSP
     *
     * @param iterations iterations number
     */
    public void solve(int iterations) {

        for (int iter = 0; iter < iterations; iter++) {
            double[][] pheromoneChange = new double[nodes][nodes];

            //N ants build route from different start nodes, N = nodes number
            for (int ant = 0; ant < nodes; ant++) {
                boolean[] freeNodes = new boolean[nodes];
                Arrays.fill(freeNodes, true);
                int visitedNodes = 1, startNode = ant, curNode = startNode;
                List<Integer> path = new ArrayList<>();
                path.add(curNode);


                while (visitedNodes < nodes) {
                    freeNodes[curNode] = false;
                    curNode = getNextNode(curNode, freeNodes);
                    visitedNodes++;
                    freeNodes[curNode] = false;
                    path.add(curNode);
                }
                path.add(startNode);
                double cost = getPathCost(path);
                saveBest(path, cost);

                //calculating matrix of pheromone changing values
                for (int i = 0; i < path.size() - 1; i++) {
                    pheromoneChange[path.get(i)][path.get(i + 1)] += Q / cost;
                    pheromoneChange[path.get(i + 1)][path.get(i)] += Q / cost;
                }
            }
            try {
                semaphore.acquire(THREADS);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }

            //changing pheromone values
            for (int i = 0; i < nodes; i++) {
                for (int j = 0; j < nodes; j++) {
                    pheromone[i][j] = pheromoneRate * pheromone[i][j] + pheromoneChange[i][j];
                }
            }
            semaphore.release(THREADS);
        }
    }

    /**
     * Method performs the specified number of iterations to solve TSP.
     * It distributes all iterations among the available threads.
     *
     * @param iterations all iterations number
     */
    public void solveMT(int iterations) {
        ExecutorService threadPool = Executors.newFixedThreadPool(THREADS);
        final int iterationsPerThread = iterations / THREADS;
        for (int i = 0; i < THREADS; i++) {
            threadPool.submit(() -> {
                solve(iterationsPerThread);
            });
        }
        threadPool.shutdown();
        try {
            threadPool.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

}

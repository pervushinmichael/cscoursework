package app.services;


import java.awt.*;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.*;

public class GraphGenerator {
    private int sideLength;
    private final int nodesNum;
    private double[][] graph;
    private List<Point> points = new ArrayList<>();


    public GraphGenerator(int sideLength, int nodesNum) {
        this.sideLength = sideLength;
        this.nodesNum = nodesNum;
        graph = new double[nodesNum][nodesNum];
    }

    public void generate() {
        for (int i = 0; i < nodesNum; i++) {
            SecureRandom rnd = new SecureRandom();
            Point p;
            while (true) {
                p = new Point(rnd.nextInt(sideLength - 10) + 5,
                        rnd.nextInt(sideLength - 10) + 5);
                if (!points.contains(p)) break;
            }
            points.add(p);
        }
        for (int i = 0; i < nodesNum; i++) {
            for (int j = i + 1; j < nodesNum; j++) {
                double cost = getLength(points.get(i), points.get(j));
                graph[i][j] = cost;
                graph[j][i] = cost;
            }
        }
    }

    private double getLength(Point p1, Point p2) {
        return sqrt(pow(abs(p1.x - p2.x), 2) + pow(abs(p1.y - p2.y), 2));
    }

    public double[][] getGraph() {
        return graph;
    }

    public List<Point> getPoints() {
        return points;
    }
}
